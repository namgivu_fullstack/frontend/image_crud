import os
from flask import Flask, render_template


APP_PORT = os.environ.get('APP_PORT', 5000) ; APP_PORT=int(APP_PORT)  # 5000 is default port for flask app
API_URL  = os.environ.get('API_URL')        ; assert API_URL, 'Envvar API_URL is required'
print(f'''
{APP_PORT=}
{API_URL=}
''')

app = Flask(__name__)

@app.route('/')
def index():
    from app_helper import dataurl  # import here to amplify we use dataurl() in jinja template
    return render_template('index.html', **locals(), **globals())

@app.route('/reset')
def reset():
    from app_helper import dataurl  # import here to amplify we use dataurl() in jinja template
    return render_template('reset.html', **locals(), **globals())


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=APP_PORT, debug=True)
