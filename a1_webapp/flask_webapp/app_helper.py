import os


def dataurl(relative_filepath_fr_flask_static_folder):
    SH                = os.path.dirname(__file__)
    FLASK_STATIC_HOME = f'{SH}/static'
    filepath          = f'{FLASK_STATIC_HOME}/{relative_filepath_fr_flask_static_folder}'
    return filecontent_to_dataurl(filepath)

def filecontent_to_dataurl(filepath):
    '''
    ref. https://stackoverflow.com/a/6375973/248616
    ref. https://stackoverflow.com/a/53688522/248616
    '''
    #region set :dataurl_prefix by file ext
    ext = filepath.split('.')[-1] ; assert ext in ['png', 'pdf', 'jpeg']

    if   ext in ['png', 'jpeg']: dataurl_prefix = f'data:image/{ext};base64,'
    elif ext in ['pdf']:         dataurl_prefix = f'data:application/pdf;base64,'
    else:                        raise Exception(f'Unsupported {ext=}')
    #endregion set :dataurl_prefix by file ext

    # parse dataurl to utf-8 string @ python
    import base64
    binary_fc       = open(filepath, 'rb').read()  # fc aka file_content
    base64_utf8_str = base64.b64encode(binary_fc).decode('utf-8')

    dataurl = f'{dataurl_prefix}{base64_utf8_str}'
    return dataurl
