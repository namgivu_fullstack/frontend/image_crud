import css+js macro to your flask template
{% from '_macro/fileupload/macro_layout.html' import fileupload_bs4row_css with context %}
{% from '_macro/fileupload/macro_script.html' import fileupload_bs4row_js with context %}

choose a bs4 container elem, a row will be added to it as a `:fileupload control` aka uploader
> eg <div class="render_as_htmlcontrol container-fluid">

`files` are stored in mongodb in `fileupload` collection, each file belong to an `owner`
an `uploader row` contain those files 's thumbnails; of course belonging to an onwer
aka
a `fileupload control` list all files uploaded by an `owner` 

filelist = GET /fileupload/byowner/:owner
then, rendered into fileupload control by 
> render_fileupload_control(filelist, owner)
>          filelist_to_html(filelist, owner)
> for              f in     filelist: 
>     html_fr_file(f.filetype, f.dataurl, f._id)

---

a sample rendered result is at this `div.render_as_htmlcontrol__static`

api ping health                        at `{{ API_URL }}/ping`
data fr api called and preview as text at `div.render_as_text`
