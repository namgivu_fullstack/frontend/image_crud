SH=`cd ${BASH_SOURCE%/*} && pwd`
AH=`cd "$SH/../.."       && pwd` ; [ -z $BASH_SOURCE ] && AH=/app
#                                ; in container, sh shell only, no bash shell -> no $BASH_SOURCE, we hardcode AH path at /app

cd $AH
PYTHONPATH=$AH \
    python3 -m pipenv run \
        python -u "$AH/flask_webapp/app.py"

sample_console_output='
$ curl 127.0.0.1:5000

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <div>flask_webapp</div>
</body>
</html>
'
