#!/bin/bash
SH=$(dirname `realpath $BASH_SOURCE`)

# delete all subfolders ref. https://unix.stackexchange.com/a/392900/17671
(cd $SH ; ls -d  */ | xargs rm -rf)
