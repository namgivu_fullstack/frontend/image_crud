from dotenv import load_dotenv
import os
import time
#
from service.selenium_webdriver import wd_vault
from service.selenium_webdriver.wd_helper import sd, ss
#
from selenium.webdriver.common.by import By
from service.selenium_webdriver.wd_helper import wait_visible, hover_mouse_over


#region TESTEE_WEBAPP_URL
load_dotenv()

TESTEE_WEBAPP_URL = os.environ.get('TESTEE_WEBAPP_URL')
assert TESTEE_WEBAPP_URL, 'Envvar TESTEE_WEBAPP_URL is required eg TESTEE_WEBAPP_URL=http://127.0.0.1:31207 _.sh'

TESTEE_MONGOEXPRESS_URL = os.environ.get('TESTEE_MONGOEXPRESS_URL')
assert TESTEE_WEBAPP_URL, 'Envvar TESTEE_MONGOEXPRESS_URL is required eg TESTEE_WEBAPP_URL=http://127.0.0.1:31205 _.sh'
#endregion TESTEE_WEBAPP_URL


wd = wd_vault.load_webdriver_f_autoinstall_chrome_wd()  #NOTE turn on when need easier debug as headless is HARD to debug AND need snapshot alot

try:
    #region reste data
    _='reset data'; wd.get(f'{TESTEE_WEBAPP_URL}/reset') ; sd();ss(wd)

    # wait till all resetdata is ready
    _='reset data'; wd.get(f'{TESTEE_MONGOEXPRESS_URL}/db/db/fileupload') ; sd();ss(wd)

    reset_res_docnum = 15
    wait_visible(wd, By.XPATH, f"//button[text()[contains(.,  'all {reset_res_docnum} doc')]]")
    #endregion reste data

    wd.get(TESTEE_WEBAPP_URL) ; sd();ss(wd)

    def check_add_file(owner):
        fileupload_div = f'.fileupload[data-owner="{owner}"]'

        def add_file(filepath):
            wd.find_element(By.CSS_SELECTOR, f'{fileupload_div} .uploader_fileformfield').send_keys(filepath)
            wd.execute_script(f'''var     e=$('{fileupload_div} .uploader_fileformfield') ; add_file(e, '{owner}'); ''')

        #region load fixture
        AH = os.path.dirname(__file__)
        filepng_namgivu3 = f'{AH}/fixture/namgivu3.png'
        filepdf_namgivu  = f'{AH}/fixture/namgivu.pdf'
        #endregion load fixture

        add_file(filepng_namgivu3) ; sd();ss(wd)
        add_file(filepdf_namgivu)  ; sd();ss(wd)

        _='reload to see newly added files' ; wd.get(TESTEE_WEBAPP_URL) ; sd();ss(wd)

        '''
        delete a) newly added file ie thumb[n-1], b) file added by :webapp/reset ie thumb[0]
        '''
        #                    =                          .fileupload   [@data-owner='xx'     ]                     .thumb_border    [data_id]
        fileupload_thumb_div = f'''//*[contains(@class, 'fileupload')][@data-owner='{owner}']//*[contains(@class, 'thumb_border')][@data_id]'''
        thumb_e_list         = wd.find_elements(By.XPATH, f'{fileupload_thumb_div}') ; n=len(thumb_e_list)
        def del_file(thumb_index):
            """
            hover the thumb to seethenclick the trashbin icon to delete
            """
            e_thumb = wd.find_element(By.XPATH, f'({fileupload_thumb_div})[{thumb_index}]')

            hover_mouse_over(wd, e_thumb)

            wait_visible(e_thumb, By.CSS_SELECTOR, '.del_thumb').click()

        del_file(thumb_index=n);   _='reload to see newly added files' ; wd.get(TESTEE_WEBAPP_URL) ; sd();ss(wd)  #CAUTION here, we must delete the last item, then the first item in lst-then-first order
        del_file(thumb_index=1);   _='reload to see newly added files' ; wd.get(TESTEE_WEBAPP_URL) ; sd();ss(wd)

    check_add_file(owner='owner0') ; sd();ss(wd)
    #TODO check_add_file(xpath='.fileupload[@data-owner="owner1"]') ;     sd();ss(wd)

except Exception:
    raise

finally:
    wd.quit()
