import os.path
#
from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
#
import time
from service.simpler_datetime import timestamp_as_id


#region slowdown if needed
GLOBAL_SLOWDOWN_SECS=.6
GLOBAL_SLOWDOWN_SECS=.1
GLOBAL_SLOWDOWN_SECS=1

def sd(secs=GLOBAL_SLOWDOWN_SECS):  # aka slow_down()
    time.sleep(secs)
#endregion slowdown if needed

#region take snapshot
SH            = os.path.abspath(os.path.dirname(__file__))
AH            = os.path.abspath(f'{SH}/../..')
SNAPSHOT_HOME = f'{AH}/zsnapshot/{timestamp_as_id(remove_millisecond=True)}' ; os.makedirs(SNAPSHOT_HOME, exist_ok=True)

def ss(wd, label='', scrollto=None):  # aka snapshot
    if scrollto:
        locate_by, locator_val = scrollto
        elem                   = wd.find_element(locate_by, locator_val)
        scroll_to(wd, elem)

    ts         = timestamp_as_id()
    snapshot_f = f'{SNAPSHOT_HOME}/{ts}_{label}.png'

    wd.save_screenshot(snapshot_f)  # this will take snapshot of what seen from the screen; may have scroll
    # wd.find_element(By.TAG_NAME, 'body').screenshot(snapshot_f)  # this helps remove the scrollbar; BUT will have flashing ref. https://stackoverflow.com/a/53825388/248616
#endregion take snapshot

#region waitxx
def wait_present(wd, by, locator, timeout=3):
    elem = WebDriverWait(wd, timeout).until(EC.presence_of_element_located((by, locator)))
    return elem


def wait_visible(wd, by, locator, timeout=3):
    elem = WebDriverWait(wd, timeout).until(EC.visibility_of_element_located((by, locator)))
    return elem


def wait_for_clickable(driver, elem_value, By_type=By.XPATH, wait_time=6):
    try:
        return WebDriverWait(driver, wait_time).until(EC.element_to_be_clickable((By_type, elem_value)))
    except Exception:
        raise Exception("Website timed out, please try again.")
#endregion waitxx


def scroll_to(wd, elem):
    wd.execute_script('arguments[0].scrollIntoView();', elem)  # ref. https://stackoverflow.com/a/41744403/248616


def hover_mouse_over(wd, elem):
    hover = ActionChains(wd).move_to_element(elem)  # ref. https://stackoverflow.com/a/8261754/248616
    hover.perform()


def scrollhoverclick(wd, elem, take_snapshot=False):
    scroll_to(wd, elem)
    hover_mouse_over(wd, elem)

    if take_snapshot:
        sd();ss(wd)

    elem.click()


def switch_tab(wd, tab_index=1):
    wd.switch_to.window(wd.window_handles[tab_index])
