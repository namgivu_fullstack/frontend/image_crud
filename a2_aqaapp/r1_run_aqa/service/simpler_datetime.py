from datetime import datetime
import re


def timestamp_as_id(remove_millisecond=False):
    now = datetime.now()
    idstr = str(now)\
        .replace(' ', '_') \
        .replace(':', '')  \
        .replace('-', '')  \
        .replace('+', 'Z')

    if remove_millisecond:
        idstr = re.sub('[.]\d+', '', idstr)
    return idstr
