SH=$(dirname `realpath $BASH_SOURCE`)
cd $SH
    PYTHONPATH=$SH \
    python3 -m pipenv run \
        python "$SH/_.py"
