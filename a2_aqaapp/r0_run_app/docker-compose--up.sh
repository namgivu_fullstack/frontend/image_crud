APP_PORT_apiapp=${APP_PORT_apiapp:-31206}
APP_PORT_webapp=${APP_PORT_webapp:-31207}
              p='imagecrudv2'

       SH=`cd ${BASH_SOURCE%/*}     && pwd`
AH_apiapp=`cd "$SH/../../a0_apiapp" && pwd`
AH_webapp=`cd "$SH/../../a1_webapp" && pwd`

# docker-compose build on the fly forced no cache ref. https://stackoverflow.com/a/61930747/248616
if [[ "$nocache" -eq "1" ]]; then
       build_context__apiapp="$AH_apiapp" \
    build_dockerfile__apiapp="$AH_apiapp/zcontainer/Dockerfile" \
    _=# \
       build_context__webapp="$AH_webapp" \
    build_dockerfile__webapp="$AH_webapp/zcontainer/Dockerfile" \
    _=# \
        docker-compose \
            -p $p \
            -f "$SH/docker-compose.yml" \
            build --no-cache
fi


   build_context__apiapp="$AH_apiapp" \
build_dockerfile__apiapp="$AH_apiapp/zcontainer/Dockerfile" \
_=# \
   build_context__webapp="$AH_webapp" \
build_dockerfile__webapp="$AH_webapp/zcontainer/Dockerfile" \
_=# \
    docker-compose \
        -p $p \
        -f "$SH/docker-compose.yml" \
        'up' \
        -d  --build \
        `#  --build force to re-create container BUT not rebuild its image`

echo ; docker ps | grep -E "$p|$APP_PORT_apiapp|$APP_PORT_webapp" --color=always

#TODO latest code of api+web app not built even we pass nocache=1 - we trying --build above, lets see if it working
