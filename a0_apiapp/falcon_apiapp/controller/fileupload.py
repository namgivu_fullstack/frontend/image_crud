import falcon
from bson import json_util
#
from falcon_apiapp import model
from falcon_apiapp.app_helper import no_ObjectId
#
from service.db.mongodb_service import mgdb


class FileUpload__byowner:

    #TODO auth
    # auth = {'auth_disabled': False}  # require JWT to call this endpoint handler/resource -> disable falcon-auth check for this endpoint handler ref. https://github.com/vertexcover-io/falcon-auth#disable-authentication-for-a-specific-resource

    def on_get(self, req, resp, owner):
        r = model.fileupload.find({'owner_id': owner})

        resp.status = falcon.HTTP_200
        resp.text   = json_util.dumps(no_ObjectId(r))


class FileUpload:

    #TODO auth
    # auth = {'auth_disabled': False}  # require JWT to call this endpoint handler/resource -> disable falcon-auth check for this endpoint handler ref. https://github.com/vertexcover-io/falcon-auth#disable-authentication-for-a-specific-resource

    def on_post(self, req, resp):
        reqbody  = req.media

        d = {
            **reqbody,
            'filename' : reqbody.get('filename'),
            'filetype' : reqbody.get('filetype'),
        }

        owner_id = d.get('owner_id') ; assert owner_id, f'Value for :owner_id is required'

        inserted = model.fileupload.POST_create(d)

        resp_d = {
            '_id' : str(inserted['_id']),

            # extra info returned bytheway eg file name+type
            'filename' : inserted.get('filename'),
            'filetype' : inserted.get('filetype'),
            'owner_id' : inserted.get('owner_id'),
        }

        resp.status = falcon.HTTP_200
        resp.text   = json_util.dumps({'inserted': resp_d})


class FileUpload__atid:

    #TODO auth
    # auth = {'auth_disabled': False}  # require JWT to call this endpoint handler/resource -> disable falcon-auth check for this endpoint handler ref. https://github.com/vertexcover-io/falcon-auth#disable-authentication-for-a-specific-resource

    def on_get(self, req, resp, _id):
        r = model.fileupload.GET_atid(_id)

        resp.status = falcon.HTTP_200
        resp.text = json_util.dumps(no_ObjectId(r))


    def on_put(self, req, resp, _id):
        resp.status = falcon.HTTP_405
        resp.text   = json_util.dumps({'message': 'ERROR 405 Method Not Allowed - no update for :file, just delete oldone and upload newone'})


    def on_delete(self, req, resp, _id):
        if not _id: raise Exception('Param _id is required')

        deleted = model.fileupload.DELETE_deleted(_id)

        resp_d = {
            '_id' : str(deleted['_id']),
        }

        resp.status = falcon.HTTP_200
        resp.text   = json_util.dumps({'deleted': resp_d})


class Fileupload_dropcollection:

    #TODO auth
    # auth = {'auth_disabled': False}  # require JWT to call this endpoint handler/resource -> disable falcon-auth check for this endpoint handler ref. https://github.com/vertexcover-io/falcon-auth#disable-authentication-for-a-specific-resource

    def on_delete(self, req, resp):
        mgdb.drop_collection(model.fileupload.cn)  # drop colleciton ref. https://stackoverflow.com/a/48923771/248616

        resp.status = falcon.HTTP_200
        resp.text   = json_util.dumps(True)
