import os

import falcon
import controller


APP_PORT = os.environ.get('APP_PORT', 8000) ; APP_PORT=int(APP_PORT)  # 8000 is default port for falcon app

app = falcon.App(cors_enable=True)

app.add_route('/ping', controller.Ping() )

app.add_route('/fileupload',                 controller.FileUpload() )
app.add_route('/fileupload/byowner/{owner}', controller.FileUpload__byowner() )
app.add_route('/fileupload/{_id}',           controller.FileUpload__atid() )
app.add_route('/fileupload_dropcollection',  controller.Fileupload_dropcollection() )

if __name__ == '__main__':
    print(f'Serving falcon apiapp at port {APP_PORT} ...')  #CAUTION to see this print() when run as container, require python -u when run this file ref. https://stackoverflow.com/a/29745541/248616
    from wsgiref.simple_server import make_server ; make_server(host='0.0.0.0', port=APP_PORT, app=app).serve_forever()  # how to have param debug=True so as to autoreload code when changes detected - similar thing in Flask webapp
