import pytz
from datetime import datetime
import re
import pymongo


#region simpler datetime util
SG_TIMEZONE = pytz.timezone('Asia/Singapore')

def sgt_now():
    return datetime.now(tz=SG_TIMEZONE)

def timestamp_as_id(ts:datetime=sgt_now()):
    idstr = str(ts)\
        .replace(' ', '_') \
        .replace(':', '')  \
        .replace('-', '')  \
        .replace('+', 'Z')
    idstr = re.sub('[.]\d+', '', idstr)
    return idstr
#endregion simpler datetime util


def no_ObjectId(d):
    #region if is a list, recursively dump its items
    if any([
        isinstance(d, pymongo.cursor.Cursor),
        isinstance(d, list),
    ]):
        return [no_ObjectId(i) for i in d]
    #endregion if is a list, recursively dump its items

    #region dump single dict
    if isinstance(d, dict):
        if '_id' in d:
            d['_id'] = str(d['_id'])
            return d
    #endregion dump single dict

    return d
