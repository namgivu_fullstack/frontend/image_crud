from service.db.mongodb_service import mgdb
from bson import ObjectId
#
from falcon_apiapp.app_helper import sgt_now


#region common method() used by :base, and the :inherit
def _POST_create(cn, d):
    _        = mgdb[cn].insert_one(d)
    inserted = mgdb[cn].find_one(_.inserted_id)
    return inserted
#endregion common method() used by :base, and the :inherit


class BaseMongodbModelclass:
    # cn = 'pls set :collection_name for your modelclass when inherit fr BaseMongodbModelclass'  # aka collection_name


    #region CRUD action

    @classmethod
    def GET_list(cls):
        r = mgdb[cls.cn].find()
        return r


    @classmethod
    def POST_create(cls, d):
        return _POST_create(cls.cn, d)


    @classmethod
    def GET_atid(cls, _id):
        r = mgdb[cls.cn].find_one({'_id': ObjectId(_id)})
        #               .                 must wrap _id when .find() ref. https://stackoverflow.com/a/8233427/248616
        return r


    @classmethod
    def PUT_update(cls, _id, d_toupdate):
        update_params = {'$set': d_toupdate}

        #region archive the updated into _history_
        '''
        when upd/del a :doc, 
        00 move :doc to mgdb[_history_] ['that :doc collname'].insert(:doc)
        01 add field .at = timestamp when upd/del happened
        '''
        d_current = mgdb[cls.cn].find_one(filter={'_id': ObjectId(_id)})

        #DEBUG @ diff
        # d_toupdate['abb'] = 122
        # d_toupdate['ccc'] = 333
        # d_toupdate['ten_donvi'] = 122

        def compare_dict(d_current, d_toupdate):
            s_current = set(d_current.items())
            s_toupdate = set(d_toupdate.items())
            s_diff = s_toupdate - s_current
            d_diff = {k: v for k, v in s_diff}
            return d_diff
        d_diff = compare_dict(d_current, d_toupdate)

        now_timestamp    = sgt_now()
        d_current['_id'] = f'{now_timestamp} {d_current["_id"]}'  # adding this to make the :doc differs - aka to dodge duplicated error <-- we have _id in :d_current

        d_history = {
            **d_current,

            '_': {
                'on'   : 'update',
                'at'   : now_timestamp,
                'diff' : d_diff,  # NOTE compare to :D_current and ONLY SAVE the newly 'change/add' fields
            },
        }
        mgdb['_history_'][cls.cn].insert_one(d_history)
        #endregion archive the updated into _history_

        # do update
        _       = mgdb[cls.cn].find_one_and_update(filter={'_id': ObjectId(_id)}, update=update_params, upsert=True)
        updated = mgdb[cls.cn].find_one(_['_id'])
        return updated


    @classmethod
    def DELETE_deleted(cls, _id):
        #region archive the deleted into _history_
        '''
        when upd/del a :doc,
        00 move :doc to mgdb[_history_] ['that :doc collname'].insert(:doc)
        01 add field .at = timestamp when upd/del happened
        '''
        d_current = mgdb[cls.cn].find_one(filter={'_id': ObjectId(_id)})
        d_history  = {
            **d_current,

            '_': {
                'on': 'delete',
                'at': sgt_now(),
            },
        }
        mgdb['_history_'][cls.cn].insert_one(d_history)
        #endregion archive the deleted into _history_

        # do delete
        deleted = mgdb[cls.cn].find_one_and_delete(filter={'_id': ObjectId(_id)})
        return deleted

    #endregion CRUD action


    @classmethod
    def find_one(cls, filter_d):
        r = mgdb[cls.cn].find_one(filter_d)
        return r


    @classmethod
    def find(cls, filter_d):
        r = mgdb[cls.cn].find(filter_d)
        return r
