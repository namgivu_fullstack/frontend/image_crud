SH=`cd ${BASH_SOURCE%/*} && pwd`
AH=`cd "$SH/../.."       && pwd` ; [ -z $BASH_SOURCE ] && AH=/app
#                                ; in container, sh shell only, no bash shell -> no $BASH_SOURCE, we hardcode AH path at /app

cd $AH
MONGO_DB_PORT=${MONGO_DB_PORT:-31204} \
PYTHONPATH=$AH \
    python3 -m pipenv run \
        python -u "$AH/falcon_apiapp/app.py"
