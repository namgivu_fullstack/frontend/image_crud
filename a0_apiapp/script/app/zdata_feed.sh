API_URL=8000

POST_fileupload() {
    d="$1"
    echo -e "\n\n--- POST /fileupload $d"; curl -X POST http://127.0.0.1:"$API_URL/fileupload" -s --data-raw "$d" --header 'Content-Type: application/json'
}

    d='{
        "owner"    : "owner0",
        "filename" : "image.png",
        "filetype" : "png",
        "dataurl"  : "data:image/png;base64,iVBORwJUST_A_SAMPLE_HERE"
    }' ; POST_fileupload "$d"

    d='{"owner": "owner1", "filename" : "image0.png", "filetype" : "png",  "dataurl"  : "data:image/png;base64,iVBORwJUST_A_SAMPLE_HERE" }' ; POST_fileupload "$d"
    d='{"owner": "owner1", "filename" : "image1.png", "filetype" : "png",  "dataurl"  : "data:image/png;base64,iVBORwJUST_A_SAMPLE_HERE" }' ; POST_fileupload "$d"
