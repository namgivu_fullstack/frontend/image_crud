echo -e '\n\n--- testing /ping';            http                          :8000/ping
                                           #curl -X GET  http://127.0.0.1:'8000/ping'

echo -e '\n\n--- testing DELETE /fileupload_dropcollection'; curl -X DELETE  http://127.0.0.1:'8000/fileupload_dropcollection'

t=/tmp/apiapp_imagecrud_POST_fileupload
echo -e '\n\n--- testing POST /fileupload'; curl -X POST http://127.0.0.1:'8000/fileupload' -s \
    -H 'Content-Type: application/json' -d '{
        "filename": "test filename",
        "filetype": "test filetype",
        "dataurl" : "data:image/png;base64,Abb122... or data:application/pdf;base64,Abb122..."
    }' | tee $t `# output to file to get the returned _id`
_id=$(python -c "import json ; d=json.load(open('$t')) ; print(d['inserted']['_id'])") ; echo -e "\n$_id"

    echo -e '\n\n--- testing GET    /fileupload/:id'; curl -X GET    http://127.0.0.1:"8000/fileupload/$_id"
    echo -e '\n\n--- testing DELETE /fileupload/:id'; curl -X DELETE http://127.0.0.1:"8000/fileupload/$_id"

#region byowner
echo -e '\n\n--- testing GET /fileupload/byowner/xx'
    #           ;                                                                                          -d  {   "owner_id" :  $o0  ,    ...
    o0='owner0' ; curl -X POST http://127.0.0.1:"8000/fileupload" -s   -H 'Content-Type: application/json' -d '{'"\"owner_id\":\"$o0\", "' "filename": "test filename", "filetype": "test filetype", "dataurl" : "data:image/png;base64,Abb122... or data:application/pdf;base64,Abb122..." }' ; echo
    o1='owner1' ; curl -X POST http://127.0.0.1:"8000/fileupload" -s   -H 'Content-Type: application/json' -d '{'"\"owner_id\":\"$o1\", "' "filename": "test filename", "filetype": "test filetype", "dataurl" : "data:image/png;base64,Abb122... or data:application/pdf;base64,Abb122..." }' ; echo

    echo; curl -X GET http://127.0.0.1:"8000/fileupload/byowner/$o0"
    echo; curl -X GET http://127.0.0.1:"8000/fileupload/byowner/$o1"
#endregion byowner
