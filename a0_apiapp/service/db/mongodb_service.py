import os
from dotenv import load_dotenv
from pymongo import MongoClient
#
import sys


"""
ref. git@github.com:mongodb-developer/pymongo-fastapi-crud.git
"""

load_dotenv()
MONGO_DB_PORT = os.environ.get('MONGO_DB_PORT') ;               assert MONGO_DB_PORT ; print(f'{MONGO_DB_PORT=} from {__file__}')
MONGO_DB_HOST = os.environ.get('MONGO_DB_HOST', '127.0.0.1') ;  assert MONGO_DB_HOST ; print(f'{MONGO_DB_HOST=} from {__file__}')
#
ATLAS_URI      = f'mongodb://root:root@{MONGO_DB_HOST}:{MONGO_DB_PORT}'  # test host:port by > nmap -p $port $host
mongodb_client = MongoClient(ATLAS_URI, timeoutMS=1000)   # open connection
#              =                      , timeoutMS set as 1 sec ref. .venv/lib/python3.9/site-packages/pymongo/mongo_client.py
#
DB_NAME      = 'db'
DB_NAME_TEST = DB_NAME+'_test'

mgdb_test      = mongodb_client[DB_NAME_TEST]  # mgdb_xx aka mongo_database_xx

#              = 00 test db                             01 real db
mgdb           = mgdb_test if 'pytest' in sys.modules   else mongodb_client[DB_NAME]
#              = if running pytest, force to use :mgdb_test ref. https://stackoverflow.com/a/44595269/248616

def mgdb_insert(c:'collection name', d:dict):
    _ = mgdb[c].insert_one(d)
    return _


def mgdb_find(c:'collection name', filter_d:dict, limit:int=None):
    r = mgdb[c].find(filter_d, limit=limit if limit else -1)
    r = list(r)  #TODO caution when grow big, this can be slow
    return r


def mgdb_drop(c:'collection name'):
    mgdb.drop_collection(c)  # NOTE that db will be dropped too if db empty after dropping
