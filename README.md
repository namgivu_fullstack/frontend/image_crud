## about
Image .png .jpg .jpeg files and .pdf files uploader as a html element/control.

Uploaded to an api endpoint storing image as dataurl base64 string; stored into mongodb database.


## ref
References the image uploader of https://www.qrcodechimp.com/qr-code-business-card

![qrcodechimp_ref.png](README%2Fqrcodechimp_ref.png)
